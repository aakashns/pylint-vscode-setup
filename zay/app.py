'''App will run here'''

from .logic import add

def use_add(num1, num2):
    '''Use add from logic'''
    return add(num1, num2)
