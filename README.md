# Python Project Structure

This is a suggested project structure to ensure that Pylint work properly with 
Visual Studio Code in a virtual environment.

## Directory Structure
```
|- requirements.txt
|- main.py
|- zay
   |- app.py
   |- logic.py
```

## Setup Instructions

Run the following commands on your terminal:
```
git clone https://gitlab.com/aakashns/pylint-vscode-setup.git
cd pylint-vscode-setup
mkvirtualenv pylint-test
pip install -r requirements.txt
code . &
python main.py
```

Relative imports are used in `logic.py` and `app.py` and absolute imports are used in `main.py`.